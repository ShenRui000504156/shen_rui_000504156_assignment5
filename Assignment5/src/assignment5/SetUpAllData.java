/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.City;
import Framingham.Community;
import Framingham.Family;
import Framingham.House;
import Framingham.Person;
import Framingham.VitalSign;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
 *
 * @author Cuishaowen
 */
public class SetUpAllData {
    public static void SetUpAll(City city){
        
        //read person infomation .xls file
        int[][] relationship = new int[1014][7];
        try{ 
            InputStream is = new FileInputStream("AllData.xls");
            jxl.Workbook rwb = Workbook.getWorkbook(is);
            Sheet rs = rwb.getSheet(0);
             
            Cell cell=rs.getCell(0, 0);
            int i=0,j=0;
            String b="";
             
            do
            {
                Person p = new Person();
                j = 0;
                 
            	cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String id=cell.getContents();
                j++;//1
                 
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String name=cell.getContents();
                p.setName(name);
                j++;//2
                
//                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
//                String year=cell.getContents();
//                p.setBirthYear(year);
//                j++;
                
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String gender=cell.getContents();
                p.setGender(gender.charAt(0));
                j++;//3
                
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String father=cell.getContents();
                if(father.trim().equals("null"))
                {
                    relationship[i][0] = 0;
                }
                else
                {
                    relationship[i][0] = Integer.parseInt(father);
                }
                j++;//4
                
                cell = rs.getCell(j, i);//第一位是列号，第二位是行号
                String mother=cell.getContents();
                if(mother.trim().equals("null"))
                {
                    relationship[i][1] = 0;
                }
                else
                {
                    relationship[i][1] = Integer.parseInt(mother);
                }
                j++;//5
                
                cell = rs.getCell(j, i);
                b = cell.getContents();
                
                for(int sibling = 2; sibling<7;sibling++)
                {
                    if(!b.trim().equals("null"))
                    {
                        relationship[i][sibling] = Integer.parseInt(b);
                        
                        if(sibling<6)
                        {
                            j++;
                            cell = rs.getCell(j, i);
                            b = cell.getContents();
                        }
                    }
                    else
                    {
                        relationship[i][sibling] = 0;
                        if(sibling<6)
                        {
                            j++;
                            cell = rs.getCell(j, i);
                            b = cell.getContents();
                        }
                    }
                }
                
                city.getCpd().addPatient(p);
                i++;
                 
                // cell=rs.getCell(0, i);
                // b=cell.getContents();
                 
            }while(i < 1013);
             
            rwb.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
//        for(int a = 0 ; a < 1013 ; a++)
//        {
//            for(int c = 0 ; c < 7 ; c++)
//            {
//                System.out.print(relationship[a][c] + " ");
//            }
//            System.out.println();
//        }
        
        ArrayList<Family> families = new ArrayList<Family>();
        
        //set one person's family information
        int pCount = 0;
        for(Person p : city.getCpd().getPersons())
        {
            int i = 0;
            if(relationship[pCount][i] != 0)
            {
                p.setFather(city.getCpd().getPersons().get(relationship[pCount][i] - 1));
            }
            i++;
            if(relationship[pCount][i] != 0)
            {
                p.setMother(city.getCpd().getPersons().get(relationship[pCount][i] - 1));
            }
            i++;
            for(int j = i ; j < 7 ; j++)
            {
                if(relationship[pCount][j] != 0)
                {
                    p.addSiblings(city.getCpd().getPersons().get(relationship[pCount][j] - 1));
                    city.getCpd().getPersons().get(relationship[pCount][j] - 1).setInFamily(true);
                }
            }
            pCount++;
            
            //set family
            if(p.isInFamily() == false)
            {
                Family f = new Family();
                f.addPerson(p);
                p.setInFamily(true);
                if(p.getFather()!=null)
                {
                    f.addPerson(p.getFather());
                }
                if(p.getMother()!=null)
                {
                    f.addPerson(p.getMother());
                }
                for(Person sib : p.getSiblings())
                {
                    f.addPerson(sib);
                }
                families.add(f);
            }
        }
        
        
        //set house
        ArrayList<House> houses = new ArrayList<House>();
        House h1 = new House();
        houses.add(h1);
        houses.get(0).addFamily(families.get(0));
        for(int i = 0 ; i < families.size() ; i++)
        {
            boolean f = false;
            for(Person pi : families.get(i).getPersons())
            {
                if(i<families.size()-1)
                {
                    for(Person pj : families.get(i+1).getPersons())
                    {
                        if(pi.getId() == pj.getId())
                        {
                            f = true;
                        }
                    }
                }
            }
            if(f == true)
            {
                houses.get(houses.size() - 1).addFamily(families.get(i+1));
            }
            else
            {
                if(i<families.size()-1)
                {
                    House h = new House();
                    houses.add(h);
                
                    houses.get(houses.size() - 1).addFamily(families.get(i+1));
                }
            }
        }
        
        for(House h : houses)
        {
            for(Family f : h.getFamilies())
            {
                for(Person p : f.getPersons())
                {
                    if(h.getHpd().getPerson().size() == 0)
                    {
                        h.getHpd().addPerson(p);
                    }
                    else
                    {
                        boolean ff = false;
                        for(int i = 0 ; i < h.getHpd().getPerson().size() ; i++)
                        {
                            if(p == h.getHpd().getPerson().get(i))
                            {
                                ff =true;
                            }  
                        }
                        if(ff == false)
                        {
                            h.getHpd().addPerson(p);
                        }
                    }
                }
            }
        }
        
//                System.out.println(houses.get(0).getFamilies().size());
//                System.out.println(houses.size());


        //set community
        for(int ii = 0 ; ii < 10 ; ii ++)
        {
            boolean f = false;
            Community community = city.addCommunity();
            for(int j = 0 ; j < 8 ; j++)
            {
                community.addHouse(houses.get(ii*8+j));
                community.getCpd().getPersons().addAll(houses.get(ii*8+j).getHpd().getPerson());
                if(ii*8+j+1 > houses.size())
                {
                    f = true;
                    break;
                }
            }
            if(f == true)
            {
                break;
            }
        }
        
        //set score information
        for(Person p : city.getCpd().getPersons())
        {
            Random rand = new Random();
            
            //set every person's vital sign
            for(int i = 0 ; i < 10 ; i++)
            {
                VitalSign vs = p.getPatient().addVitalSigns();
                
                int randomSBP = rand.nextInt((200-110)+1)+110;
                vs.setSbp(randomSBP);
                int randomDBP = rand.nextInt((110-50)+1)+50;
                vs.setDbp(randomDBP);
                double randomTC = rand.nextDouble() * 9 + 1;
                vs.setTotalCholesterol(randomTC);
                double randomHDLC = rand.nextDouble() * 2;
                vs.setHdlCholesterol(randomHDLC);
                
                vs.getDate().setYear(2016);
                vs.getDate().setMonth(10);
                vs.getDate().setDay(15 + i);
                int randomHour = rand.nextInt((23-0)+1);
                vs.getDate().setHour(randomHour);
                int randomMinite = rand.nextInt((59-0)+1);
                vs.getDate().setMinite(randomMinite);
            }
            
            //set smoker
            double randomSmoker = rand.nextDouble();
            if(randomSmoker > 0.5)
            {
                p.getPatient().setSmoker(true);
            }
            else
            {
                p.getPatient().setSmoker(false);
            }
            
            //set diabetes
            double randomDiabetes = rand.nextDouble();
            if(randomDiabetes > 0.5)
            {
                p.getPatient().setDiabetes(true);
            }
            else
            {
                p.getPatient().setDiabetes(false);
            }
            
            //set age
            if(p.getFather() == null)
            {
                if(p.getMother() == null)
                {
                    continue;
                }
                else if(p.getMother().getFather() == null && p.getMother().getFather() == null)
                {
                    continue;
                }
            }
            else if(p.getMother() == null)
            {
                if(p.getFather().getFather() == null && p.getFather().getMother() == null)
                {
                    continue;
                }
            }
//            else if(p.getFather() == null && p.getMother().getFather() == null && p.getMother().getMother() == null)
//            {
//                continue;
//            }
//            else if(p.getMother() == null && p.getFather().getFather() == null && p.getFather().getMother() == null)
//            {
//                continue;
//            }
//            else if(p.getFather().getFather() == null && p.getFather().getMother() == null && p.getMother().getFather() == null && p.getMother().getMother() == null) 
//            {
//                continue;
//            }
            else
            {
                int randomAge = rand.nextInt((45-20)+1)+20;
                p.setAge(randomAge);
                if(p.getFather() != null && p.getFather().getAge() == 0)
                {
                    p.getFather().setAge(randomAge + 30);
                    if(p.getFather().getFather() != null && p.getFather().getFather().getAge() == 0)
                    {
                        p.getFather().getFather().setAge(randomAge + 60);
                    }
                    if(p.getFather().getMother() != null && p.getFather().getMother().getAge() == 0)
                    {
                        p.getFather().getMother().setAge(randomAge + 60);
                    }
                }
                if(p.getMother() != null && p.getMother().getAge() == 0)
                {
                    p.getMother().setAge(randomAge + 30);
                    if(p.getMother().getFather() != null && p.getMother().getFather().getAge() == 0)
                    {
                        p.getMother().getFather().setAge(randomAge + 60);
                    }
                    if(p.getMother().getMother() != null && p.getMother().getMother().getAge() == 0)
                    {
                        p.getMother().getMother().setAge(randomAge + 60);
                    }
                }
                
                
            }
        }
        
        for(Person p : city.getCpd().getPersons())
        {
            int personScore=caculateRisk.caculateRisk(p);
            p.setScore(personScore);
            if(personScore<=-1){
                p.setRisk("2%");
            }
            else if(personScore==0){
                p.setRisk("3%");
            }
            else if(personScore==1){
                p.setRisk("3%");
            }
            else if(personScore==2){
                p.setRisk("4%");
            }
            else if(personScore==3){
                p.setRisk("5%");
            }
            else if(personScore==4){
                p.setRisk("7%");
            }
            else if(personScore==5){
                p.setRisk("8%");
            }
            else if(personScore==6){
                p.setRisk("10%");
            }
            else if(personScore==7){
                p.setRisk("13%");
            }
            else if(personScore==8){
                p.setRisk("16%");
            }
            else if(personScore==9){
                p.setRisk("20%");
            }
            else if(personScore==10){
                p.setRisk("25%");
            }
            else if(personScore==11){
                p.setRisk("31%");
            }
            else if(personScore==12){
                p.setRisk("37%");
            }
            else if(personScore==13){
                p.setRisk("45%");
            }
            else if(personScore>=14){
                p.setRisk("≥53%");
            }
        }
        
    }
}
