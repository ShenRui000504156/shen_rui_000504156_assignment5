/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.City;
import Framingham.Person;
import Framingham.VitalSign;
import java.util.Scanner;

/**
 *
 * @author Cuishaowen
 */
public class Assignment5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        City city = new City();
        
        SetUpAllData.SetUpAll(city);
               
        Scanner reader = new Scanner(System.in);
        boolean b = true;
        
        while(b){
                System.out.println("*************************** Degree Performence Report ***************************");
                System.out.println("1. View Framingham Risk Score Report on Different Levels");
                System.out.println("2. View the Rate of Having Over 25 Framingham Risk Score People on Different levels");
                System.out.println("3. Compare High Risk Male and High Risk Female Framingham Risk Score Report on Different Levels");
                System.out.println("4. Calculate the Framingham Risk Score for you");
                System.out.println("0. Exit.");
                System.out.printf("Please choose a number above: ");

                int choice = reader.nextInt(); 

                switch(choice)
                {
                    case 1: boolean b1 = true;
                            while(b1){
                                System.out.println("*************************** Compare Level ***************************");
                                System.out.println("************************** Framingham Risk Score **************************");
                                System.out.println("1. Between Family.");
                                System.out.println("2. Between House.");
                                System.out.println("3. Between Community.");
                                System.out.println("0. Return to the Main Menu.");
                                System.out.printf("Please choose a number above: ");
                                int c1 = reader.nextInt();
                                switch(c1)
                                {
                                    case 1: FRS.familyFRS(city);break;
                                    case 2: FRS.houseFRS(city);break;
                                    case 3: FRS.communityFRS(city);break;
                                    case 0: b1 = false;break;
                                    default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
                                }
                            }
                            break;
                    case 2: boolean b2 = true;
                            while(b2){
                                System.out.println("*************************** Compare Level ***************************");
                                System.out.println("******* Rate of High HeartAttack Risk People *******");
                                System.out.println("1. Between Family.");
                                System.out.println("2. Between House.");
                                System.out.println("3. Between Community.");
                                System.out.println("0. Return to the Main Menu.");
                                System.out.printf("Please choose a number above: ");
                                int c1 = reader.nextInt();
                                switch(c1)
                                {
                                    case 1: HighHeartAttackRate.fmailyHighHeartAttackRate(city);break;
                                    case 2: HighHeartAttackRate.houseHighHeartAttackRate(city);break;
                                    case 3: HighHeartAttackRate.communityHighHeartAttackRate(city);break;
                                    case 0: b2 = false;break;
                                    default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
                                }
                            }
                            break;
                    case 3: boolean b3 = true;
                            while(b3){
                                System.out.println("*************************** Compare Level ***************************");
                                System.out.println("************************** Male and Female Framingham Risk Score Report *************************");
                                System.out.println("1. Between Family.");
                                System.out.println("2. Between House.");
                                System.out.println("3. Between Community.");
                                System.out.println("0. Return to the Main Menu.");
                                System.out.printf("Please choose a number above: ");
                                int c1 = reader.nextInt();
                                switch(c1)
                                {
                                    case 1: MaleFemaleRate.FamilyHeartAttackRate(city);break;
                                    case 2: MaleFemaleRate.HouseHeartAttackRate(city);break;
                                    case 3: MaleFemaleRate.CommunityHeartAttackRate(city);break;
                                    case 0: b3 = false;break;
                                    default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
                                }
                            }
                            break;
                    case 4: System.out.println("******Calculate the Framingham Risk Score for you******");
                            Person p = new Person();
                            VitalSign vs = p.getPatient().addVitalSigns();
                            
                            int age;
                            boolean f1 = false;
                            do
                            {
                                System.out.print("Please enter your Age:");
                                age = reader.nextInt();
                                if(age>=20 && age<=105)
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Your age should be between 20 - 105");
                                }                                
                            }while(f1 == false);
                            p.setAge(age);
                            f1 = false;

                            
                            String smoker;
                            do
                            {
                                System.out.println("Are you a smoker?");
                                System.out.println("Please enter Y/N");               
                                smoker = reader.next();
                                if (smoker.equals("Y") || smoker.equals("N"))
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Please enter Y/N");
                                }
                                                               
                            }while(f1 == false);
                            if (smoker.equals("Y"))
                                {
                                    p.getPatient().setSmoker(true);
                                }
                            if (smoker.equals("N")){
                                    p.getPatient().setSmoker(false);
                                }
                            f1 = false;    
                            
                            
                            String diabetes;
                            do
                            {
                                System.out.println("Do you have diabetes issue?");
                                System.out.println("Please enter Y/N");            
                                diabetes = reader.next();
                                if (diabetes.equals("Y") || diabetes.equals("N"))
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Please enter Y/N");
                                }                                                               
                            }while(f1 == false);
                            if (diabetes.equals("Y"))
                                {
                                    p.getPatient().setSmoker(true);
                                }
                            if (diabetes.equals("N")){
                                p.getPatient().setSmoker(false);
                                }
                            f1 = false;     
                                
                                                     
                            int sbp;
                            do
                            {
                                System.out.println("Please enter your sbp");
                                sbp = reader.nextInt();
                                if (sbp < 200 && sbp> 110)
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Your sbp should be between 110 - 200");
                                }
                            }while(f1 == false);                            
                            vs.setSbp(sbp);
                            f1 = false;
                            
                            
                            int dbp;
                            do
                            {
                                System.out.println("Please enter your dbp");
                                dbp = reader.nextInt();
                                if (dbp > 50 && dbp < 110)
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Your dbp should be between 50 -110");
                                }
                            }while(f1 == false);                            
                            vs.setDbp(dbp);
                            f1 = false;
                            
                            
                            double cholesterol;
                            do
                            {
                                System.out.println("Please enter your total cholesterol");
                                cholesterol = reader.nextDouble();
                                if (cholesterol > 1 && cholesterol < 10)
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Your cholesterol should be between 1 -10");
                                }
                            }while(f1 == false);                            
                            vs.setTotalCholesterol(cholesterol);
                            f1 = false;    
                            
                            
                            double hdl;
                            do
                            {
                                System.out.println("Please enter your hdlCholesterol");
                                hdl = reader.nextDouble();
                                if (hdl > 0 && hdl < 2)
                                {
                                    f1 = true;
                                }
                                else
                                {
                                    System.out.println("Your hdlCholesterol should be between 0 -2");
                                }
                            }while(f1 == false);                            
                            vs.setHdlCholesterol(hdl);
                            f1 = false;
                            
                            int t = caculateRisk.caculateRisk(p);
                            System.out.println("Your Framingham Risk Score is " + t);
                            
                            break;
                    case 0: b = false;break;
                    default: System.out.println("Please input a number from 1 to 4!");System.out.println();System.out.println();
            }
        }
    }
    
}
