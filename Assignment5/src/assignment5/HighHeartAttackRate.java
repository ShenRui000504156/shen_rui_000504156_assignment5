/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.City;
import Framingham.Community;
import Framingham.Family;
import Framingham.House;
import Framingham.Person;

/**
 *
 * @author Rui
 */
public class HighHeartAttackRate {
    
    public static void fmailyHighHeartAttackRate(City city)
    {
        
        
        for(Community c : city.getCommunities())
        {
            for(House h : c.getHouses())
            {
                for (Family f : h.getFamilies())
                {
                    double familyhighrate;
                    int familyNum = 0;
                    int highriskpeopleNum = 0;
                    for (Person p : f.getPersons())
                    {
                        familyNum = familyNum + 1;
                        if (p.getScore() > 10)
                        {
                            highriskpeopleNum = highriskpeopleNum + 1;
                        }
                    }
                    double fn = familyNum;
                    double hrpn = highriskpeopleNum;
                    familyhighrate = hrpn/fn;
                    System.out.println(" The Rate of High Framingham Risk Score of the " + f.getFamilyID() + " family is " + familyhighrate);
                }
            }
        }
        System.out.println();
    }
    
    public static void houseHighHeartAttackRate(City city)
    {
        
        for(Community c : city.getCommunities())
        {
            for(House h : c.getHouses())
            {
                double houseNum = 0;
                double highriskpeopleNum = 0;
                double househighrate;
                
                for (Person p : h.getHpd().getPerson())
                {
                    houseNum = houseNum + 1;
                    if (p.getScore() > 10)
                    {
                        highriskpeopleNum = highriskpeopleNum + 1;
                    }
                }    
                househighrate = highriskpeopleNum/houseNum;
                System.out.println(" The Rate of High Framingham Risk Score of the " + h.getHouseID() + " house is " + househighrate);     
            }
            
        }
        System.out.println();
    }

    public static void communityHighHeartAttackRate(City city)
    {
        
        for(Community c : city.getCommunities())
        {
            double communityNum = 0;
            double highriskpeopleNum = 0;
            double communityhighrate;
            
            for(Person p : c.getCpd().getPersons())
            {
                communityNum = communityNum + 1;
                if (p.getScore() > 10)
                {
                    highriskpeopleNum = highriskpeopleNum + 1;
                }
            }
            communityhighrate = highriskpeopleNum/communityNum;
            System.out.println(" The Rate of High Framingham Risk Score of the " + c.getCommunityID() + " community is " + communityhighrate);
        }
        System.out.println();
    }
    
}
