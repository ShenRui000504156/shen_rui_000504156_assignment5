/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.City;
import Framingham.Community;
import Framingham.Family;
import Framingham.House;
import Framingham.Person;

/**
 *
 * @author Rui
 */
public class MaleFemaleRate {
    
    public static void FamilyHeartAttackRate(City city)
    {
        for(Community c : city.getCommunities())
        {
            for(House h : c.getHouses())
            {
                for (Family f : h.getFamilies())
                {
                    double femaleAttackNum = 0;
                    double femaleSum = 0;
                    double maleAttackNum = 0;
                    double maleSum = 0;
                    double femaleRate;
                    double maleRate;
                    for (Person p : f.getPersons())
                    {
                        if (p.getScore()>10 && Character.toString(p.getGender()).matches("F"))
                        {
                            femaleAttackNum = femaleAttackNum + 1;
                            femaleSum += p.getScore();
                        }
                        if (p.getScore()>10 && Character.toString(p.getGender()).matches("M"))
                        {
                            maleAttackNum = maleAttackNum + 1;
                            maleSum += p.getScore();
                        }
                    }
                    femaleRate = femaleSum/femaleAttackNum;
                    maleRate = maleSum/maleAttackNum;
                    
                    System.out.println();
                    System.out.println("The High Framingham Risk Score of Female in family " + f.getFamilyID() + " is " +  femaleRate);
                    System.out.println("The High Framingham Risk Score of Male in family " + f.getFamilyID() + " is " +  maleRate);
                    System.out.println();

                }
            }
        }
        
        

    }
    
    public static void HouseHeartAttackRate(City city)
    {
        
        for(Community c : city.getCommunities())
        {
            for(House h : c.getHouses())
            {
                double femaleAttackNum = 0;
                double femaleSum = 0;
                double maleAttackNum = 0;
                double maleSum = 0;
                double femaleRate;
                double maleRate;
                for (Person p : h.getHpd().getPerson())
                {
                    if (p.getScore() > 10 && Character.toString(p.getGender()).matches("F")) {
                        femaleAttackNum = femaleAttackNum + 1;
                        femaleSum += p.getScore();
                    }
                    if (p.getScore() > 10 && Character.toString(p.getGender()).matches("M")) {
                        maleAttackNum = maleAttackNum + 1;
                        maleSum += p.getScore();
                    }
                }
                femaleRate = femaleSum/femaleAttackNum;
                maleRate = maleSum/maleAttackNum;

                System.out.println();
                System.out.println("The High Framingham Risk Score of Female in house " + h.getHouseID() + " is " + femaleRate);
                System.out.println("The High Framingham Risk Score of Male in house " + h.getHouseID() + " is " + maleRate);
                System.out.println();
            }
        }
        
        

    }
    
    public static void CommunityHeartAttackRate(City city)
    {
        
        for(Community c : city.getCommunities())
        {
            double femaleAttackNum = 0;
            double femaleSum = 0;
            double maleAttackNum = 0;
            double maleSum = 0;
            double femaleRate;
            double maleRate;
            for (Person p : c.getCpd().getPersons()) 
            {
                if (p.getScore() > 10 && Character.toString(p.getGender()).matches("F")) 
                {
                    femaleAttackNum = femaleAttackNum + 1;
                    femaleSum += p.getScore();
                }
                if (p.getScore() > 10 && Character.toString(p.getGender()).matches("M")) 
                {
                    maleAttackNum = maleAttackNum + 1;
                    maleSum += p.getScore();
                }
            } 
            femaleRate = femaleSum / femaleAttackNum;
            maleRate = maleSum / maleAttackNum;

            System.out.println();
            System.out.println("The High Framingham Risk Score of Female in community " + c.getCommunityID() + " is " + femaleRate);
            System.out.println("The High Framingham Risk Score of Male in community " + c.getCommunityID() + " is " + maleRate);
            System.out.println();
        }
        
        

    }
    
}
