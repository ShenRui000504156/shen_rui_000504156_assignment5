/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.Person;
import Framingham.VitalSign;

/**
 *
 * @author fjj1213
 */
public class caculateRisk {
    
    public static int caculateRisk(Person p){
        double averageCholesterol=0;
        double sumCholesterol=0;
        double averageHBP=0;
        double sumHBP=0;
        double averageSBP=0;
        double sumSBP=0;
        double sumhdlCholesterol=0;
        double averagehdlCholesterol=0;
       
    for(VitalSign v:p.getPatient().getVitalSigns()){
           sumCholesterol=v.getTotalCholesterol()+sumCholesterol;
           sumHBP= v.getHdlCholesterol()+sumHBP;
           sumSBP=v.getSbp()+sumSBP;
           sumhdlCholesterol=v.getHdlCholesterol()+sumhdlCholesterol;
    
     }
    averageCholesterol= sumCholesterol/p.getPatient().getVitalSigns().size();
    averageHBP= sumHBP/p.getPatient().getVitalSigns().size();
    averageSBP=sumSBP/p.getPatient().getVitalSigns().size();
    averagehdlCholesterol=sumhdlCholesterol/10;
    
    int caculateRisk=0;
    
    if(p.getAge() < 34){
        caculateRisk=-1;
    }
    else if(p.getAge()>35 && p.getAge()<=39){
        caculateRisk=0;
    }
    else if(p.getAge()>40 && p.getAge()<=44){
        caculateRisk=1;
    }
    else if(p.getAge()>45 && p.getAge()<=49){
        caculateRisk=2;
    }
    else if(p.getAge()>50 && p.getAge()<=54){
        caculateRisk=3;
    }
    else if(p.getAge()>55 && p.getAge()<=59){
        caculateRisk=4;
    }
    else if(p.getAge()>60 && p.getAge()<=64){
        caculateRisk=5;
    }
    else if(p.getAge()>60 && p.getAge()<=64){
        caculateRisk=5;
    }
    else if(p.getAge()>65 && p.getAge()<=69){
        caculateRisk=6;
    }
    else if(p.getAge()>70){
        caculateRisk=7;    
    }
    
    
    if(averageCholesterol<=4.14){
          caculateRisk=caculateRisk-3;
        }
    else if(averageCholesterol>4.14 && averageCholesterol<=5.17){
          caculateRisk=caculateRisk+0;    
        }
    else if(averageCholesterol>5.18 && averageCholesterol<=6.21){
          caculateRisk=caculateRisk+1;    
        }
    else if(averageCholesterol>6.22 && averageCholesterol<=7.24){
          caculateRisk=caculateRisk+2;    
        }
    else if(averageCholesterol>7.25){
          caculateRisk=caculateRisk+3;    
        }
    
    
    
    if(averagehdlCholesterol<=0.9){
          caculateRisk=caculateRisk+2;
        }
    else if(averagehdlCholesterol>0.91 && averagehdlCholesterol<=1.16){
          caculateRisk=caculateRisk+1;    
        }
    else if(averagehdlCholesterol>1.17 && averagehdlCholesterol<=1.29){
          caculateRisk=caculateRisk+0;    
        }
    else if(averagehdlCholesterol>1.30 && averagehdlCholesterol<=1.55){
          caculateRisk=caculateRisk+0;    
        }
    else if(averagehdlCholesterol>1.56){
          caculateRisk=caculateRisk-2;    
        }
    
   int HBP=0; 
   if(averageHBP<=80){
       HBP=HBP+0;
    }
   
   else if(averageHBP<84 && averageHBP>80){
       HBP=HBP+0;
   }
   
   else if(averageHBP<89 && averageHBP>=85){
        HBP=HBP+1;
    }
   
   else if(averageHBP<99 && averageHBP>=90){
        HBP=HBP+2;
    }
   
   else if(averageHBP>=100){
        HBP=HBP+3;
    }
   
   
   int SBP=0;
   if(averageSBP<=120){
       SBP=SBP+0;
   }
   else if(averageSBP>120 && averageSBP<=129){
       SBP=SBP+0;
   }
   else if(averageSBP>=130 && averageSBP<=139){
        SBP=SBP+1;
    }
   else if(averageSBP>=140 && averageSBP<=159){
        SBP=SBP+2;
    }
   else if(averageSBP>=160){
        SBP=SBP+3;
    }
   
   if(HBP>=SBP){
       caculateRisk=caculateRisk+HBP;
   }
   else {
       caculateRisk=caculateRisk+SBP;
   }
   
   if(p.getPatient().isDiabetes()==true){
        caculateRisk=caculateRisk+2;
   }
   else{
       caculateRisk=caculateRisk+0;
   }
   if(p.getPatient().isSmoker()==true){
       caculateRisk=caculateRisk+2;
   }
   else{
       caculateRisk=caculateRisk+0;
   }
   
   
   
   
    /*if(averageHBP<=80 && averageSBP<=120){
        caculateRisk=caculateRisk+0;
    }
    if(averageHBP<84 && averageHBP>80 && averageSBP>120 && averageSBP<=129){
        caculateRisk=caculateRisk+0;
    }
    if(averageHBP<89 && averageHBP>=85 && averageSBP>=130 && averageSBP<=139){
        caculateRisk=caculateRisk+1;
    }
    if(averageHBP<99 && averageHBP>=90 && averageSBP>=140 && averageSBP<=159){
        caculateRisk=caculateRisk+2;
    }
    
    if(averageHBP>=100 && averageSBP>=160){
        caculateRisk=caculateRisk+3;
    }*/
    
    
    
    return caculateRisk;
    
  }
}
