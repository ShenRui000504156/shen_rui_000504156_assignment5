/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment5;

import Framingham.City;
import Framingham.Community;
import Framingham.Family;
import Framingham.House;
import Framingham.Person;
import java.util.ArrayList;

/**
 *
 * @author Rui
 */
public class FRS {

    public static void familyFRS(City city) {
        double avg;
        for (Community c : city.getCommunities()) {
            for (House h : c.getHouses()) {
                for (Family f : h.getFamilies()) {
                    int familynum = 0;
                    int sum = 0;
                    for (Person p : f.getPersons()) {
                        sum += p.getScore();
                        familynum += 1;
                    }
                    avg = sum / familynum;
                    System.out.println("The average Framingham Risk Score between families is " + avg);
                }
            }
        }
        System.out.println();
    }

    public static void houseFRS(City city) {
        double avg;

        
        for (Community c : city.getCommunities()) {
            for (House h : c.getHouses()) {
                int housenum = 0;
                int sum = 0;
                for (Person p : h.getHpd().getPerson()) {
                    sum = sum + p.getScore();
                    housenum = housenum + 1;
                }
                avg = sum / housenum;
                System.out.println("The average Framingham Risk Score between houses is " + avg);
            }
        }
        System.out.println();
    }

    public static void communityFRS(City city) {
        double avg;

        
        for (Community c : city.getCommunities()) {
            int communitynum = 0;
            int sum = 0;
            for (Person p : c.getCpd().getPersons()) {
                sum = sum + p.getScore();
                communitynum = communitynum + 1;
            }
            avg = sum / communitynum;
            System.out.println("The average Framingham Risk Score between community is " + avg);
        }
        System.out.println();
    }
}
