/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Patient {
    
    private boolean smoker;
    private boolean diabetes;           //糖尿病
    
    private ArrayList<VitalSign> vitalSigns;
    
    private int healthScore;

    public Patient() {
        vitalSigns = new ArrayList<VitalSign>();
    }

    public boolean isSmoker() {
        return smoker;
    }

    public void setSmoker(boolean smoker) {
        this.smoker = smoker;
    }

    public boolean isDiabetes() {
        return diabetes;
    }

    public void setDiabetes(boolean diabetes) {
        this.diabetes = diabetes;
    }

    public ArrayList<VitalSign> getVitalSigns() {
        return vitalSigns;
    }

    public int getHealthScore() {
        return healthScore;
    }

    public void setHealthScore(int healthScore) {
        this.healthScore = healthScore;
    }
    
    public VitalSign addVitalSigns(){
        VitalSign v = new VitalSign();
        vitalSigns.add(v);
        return v;
    }
    
}
