/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class Family {
    private ArrayList<Person> persons;
    private int familyID;

    private static int fID;
    
    public Family() {
        persons = new ArrayList<Person>();
        fID++;
        familyID = fID;
    }

    public int getFamilyID() {
        return familyID;
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }
    
    public void addPerson(Person p){
        persons.add(p);
    }
}
