/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Framingham;

import java.util.ArrayList;

/**
 *
 * @author Cuishaowen
 */
public class CityPersonDirectory {
    private ArrayList<Person> persons;

    public CityPersonDirectory() {
        persons = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }
    
    public void addPatient(Person p){
        persons.add(p);
    }
    
}
